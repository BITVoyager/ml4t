"""
@brief  This file is modified from the Tucker's template.
@author Yao Chen
@date   2016-06-13
"""

import numpy as np
import matplotlib.pyplot as plt

import math
import timeit

import KNNLearner as knn
import BagLearner as bl

if __name__ == "__main__":
    tic = timeit.default_timer()

    inf = open('Data/ripple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])

    # compute how much of the data is training and testing
    train_rows = int(math.floor(0.6 * data.shape[0]))
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows, 0:-1]
    trainY = data[:train_rows, -1]
    testX = data[train_rows:, 0:-1]
    testY = data[train_rows:, -1]
    
    # loop over all possible number of bags
    nrBags = 50
    trainError = np.empty([nrBags, 1])
    testError = np.empty([nrBags, 1])
    for j in range(0, nrBags):
        print "Processing bags = ", j + 1

        learner = bl.BagLearner(learner = knn.KNNLearner, kwargs = {"k": 3}, bags = j + 1, boost = False, verbose = False) # create a LinRegLearner
        learner.addEvidence(trainX, trainY) # train it

        # evaluate the kNN regression
        predY = learner.query(trainX)  # get the predictions
        rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
        trainError[j, 0] = rmse

        predY = learner.query(testX)    # get the predictions
        rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
        testError[j, 0] = rmse

    # plot the results
    curve1, = plt.plot(trainError, color = 'blue', lw = 1)
    curve2, = plt.plot(testError, color = 'red', lw = 1)
    plt.xlabel('number of bags')
    plt.ylabel('rmse')
    plt.legend([curve1, curve2], ['training', 'testing'])
    plt.title('Training and testing errors over the ripple dataset')
    plt.show()

    toc = timeit.default_timer()
    print
    print "The run time is ", toc - tic 
