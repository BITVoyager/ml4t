"""
@brief  Generate f(x) = 2 * x2 + x1 + 6 with Gaussian noise. 
        In LinRegLearner, the dimension of data is 3 because 1s will be added automatically.
        In KNNLearner, the dimension is 2. The dimension of data is not an issue in regression.
@author Yao Chen
@date   2016-06-13
"""

import numpy as np

if __name__ == "__main__":
    # create all x's
    x2 = np.linspace(start = 0.0, stop = 40.0, num = 1200).reshape([-1, 1])
    x1 = np.linspace(start = 5.0, stop = 50.0, num = 1200).reshape([-1, 1])

    # apply the linear equation and add some noise
    y = 2.0 * x2 + x1 + 6.0 + np.random.normal(loc = 0.0, scale = 12.0, size = x2.shape)
    data = np.concatenate((x2, x1, y), axis = 1)

    # shuffle the data
    np.random.shuffle(data)

    # save the data to /Data/best4linreg.csv
    np.savetxt('./Data/best4linreg.csv', data, delimiter = ',')


