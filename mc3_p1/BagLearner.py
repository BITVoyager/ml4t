"""
@brief  This class implements a Bootstrap Aggregating (Bagging) learner for regression.
@author Yao Chen
@date   2016-06-11
"""

import numpy as np

import LinRegLearner as lrl
import KNNLearner as knn

class BagLearner(object):
	
    def __init__(self, learner = knn.KNNLearner, kwargs = {"k":3}, bags = 20, boost = False, verbose = False):
        """
        @brief  Initialize parameters.
        @param  learner: the learner (default: kNNLearner)
        @param  kwargs: arguments for the learners (default: k=3)
        @param  bags: number of bags
        @param  boost: flag of using boosting
        @param  verbose: flag of printing debug information
        """
        # initialize the number of bags
        self.bags = bags
        
        # initialize all learners with the specified parameter list
        # This seems a little bit expensive but will yield a consistent API
        self.learners = []
        for k in range(0, self.bags):
            self.learners.append(learner(**kwargs))
        
        # flag of using boosting
        self.useBoosting = boost

        # flage of printing debug information
        self.verbose = verbose

    def addEvidence(self, trainX, trainY):
        """
        @brief  Add training data to the learner.
        @param  trainX: X values of the training data
        @param  trainY: Y values of the training data
        """
        
        # define two members to hold all training data for bagging        
        self.trainX = []
        self.trainY = []

        # prepare all training data
        for j in range(0, self.bags):
            # create two empty matrices for data
            tempTrainX = np.empty(trainX.shape)
            tempTrainY = np.empty(trainY.shape)

            # apply random sampling with replacement
            for k in range(0, trainX.shape[0]):
                index = np.random.randint(low = 0, high = trainX.shape[0])
                tempTrainX[k] = trainX[index]
                tempTrainY[k] = trainY[index]

            self.trainX.append(tempTrainX)
            self.trainY.append(tempTrainY)
        
    def query(self, testX):
        """
        @brief	Estimate a set of test points given the kNN model.
        @param 	testX: the input points for testing
        @return	testY: the estimated values according to the kNN model
        """
	
        # define the predictions
        testY = np.zeros(testX.shape[0])

        # implement bagging here
        for k in range(0, self.bags):
            self.learners[k].addEvidence(self.trainX[k], self.trainY[k])
            testY += self.learners[k].query(testX)

        # return the predictions
        return testY / self.bags
        

