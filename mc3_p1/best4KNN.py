"""
@brief  Generate f(x) = 2 * (x2 - 10) ^ 2 + (x1 - 25) ^ 2 + 1. 
        In LinRegLearner, the dimension of data is 3 because 1s will be added automatically.
        In KNNLearner, the dimension is 2. The dimension of data is not an issue in regression.
@author Yao Chen
@data   2016-06-13
"""

import numpy as np

if __name__ == "__main__":
    # create all x's
    x2 = np.linspace(start = 0.0, stop = 20.0, num = 1200).reshape([-1, 1])
    x1 = np.linspace(start = 5.0, stop = 45.0, num = 1200).reshape([-1, 1])

    # apply the linear equation and add some noise
    y = 2.0 * (x2 - 10.0) * (x2 - 10.0) + (x1 - 25.0) * (x1 - 25.0) + 1.0
    data = np.concatenate((x2, x1, y), axis = 1)

    # shuffle the data
    np.random.shuffle(data)

    # save the data to /Data/best4KNN.csv
    np.savetxt('./Data/best4KNN.csv', data, delimiter = ',')


