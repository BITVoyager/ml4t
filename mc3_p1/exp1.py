"""
@brief  This file is modified from the Tucker's template.
@author Yao Chen
@date   2016-06-13
"""

import numpy as np

import math
import timeit

import LinRegLearner as lrl
import KNNLearner as knn

if __name__ == "__main__":
    tic = timeit.default_timer()

    inf = open('Data/best4linreg.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf.readlines()])

    # compute how much of the data is training and testing
    train_rows = int(math.floor(0.6 * data.shape[0]))
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows, 0:-1]
    trainY = data[:train_rows, -1]
    testX = data[train_rows:, 0:-1]
    testY = data[train_rows:, -1]

    # create a linear regression learner and train it
    learner1 = lrl.LinRegLearner(verbose = False) # create a LinRegLearner
    learner1.addEvidence(trainX, trainY) # train it

    # evaluate the linear regression
    predY = learner1.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print "Start evaluating the linear regression..."
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    predY = learner1.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]
    print "Done evaluating the linear regression."
    print
    
    # create a kNN regression learner and train it
    learner2 = knn.KNNLearner(100, verbose = False) # create a LinRegLearner
    learner2.addEvidence(trainX, trainY) # train it

    # evaluate the kNN regression
    predY = learner2.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print "Start evaluating the kNN regression..."
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]

    predY = learner2.query(testX)    # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    print "Done evaluating the kNN regression."

    toc = timeit.default_timer()
    print
    print "The run time is ", toc - tic 
