"""
Test a learner.  (c) 2015 Tucker Balch
"""

import numpy as np

import math
import timeit

import LinRegLearner as lrl
import KNNLearner as knn
import BagLearner as bl

if __name__=="__main__":
    tic = timeit.default_timer()
    inf1 = open('test_cases/simple.csv')
    data = np.array([map(float,s.strip().split(',')) for s in inf1.readlines()])

    inf2 = open('test_cases/testcase02.csv')
    testingData = np.array([map(float,s.strip().split(',')) for s in inf2.readlines()])

#    trainX = trainingData[:, 0:-1]
#    trainY = trainingData[:, -1]
#    testX = testingData[:, 0:-1]
#    testY = testingData[:, -1]

    # compute how much of the data is training and testing
    train_rows = math.floor(0.6* data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data
    trainX = data[:train_rows,0:-1]
    trainY = data[:train_rows,-1]
    testX = data[train_rows:,0:-1]
    testY = data[train_rows:,-1]

    print testX.shape
    print testY.shape

    # create a learner and train it
#    learner = lrl.LinRegLearner(verbose = True) # create a LinRegLearner
#    learner.addEvidence(trainX, trainY) # train it

#    learner = knn.KNNLearner(verbose = False) # create a LinRegLearner
#    learner.addEvidence(trainX, trainY) # train it

    learner = bl.BagLearner(learner = lrl.LinRegLearner, kwargs = {"k": 3}) # create a LinRegLearner
    learner.addEvidence(trainX, trainY) # train it

    # evaluate in sample
    predY = learner.query(trainX) # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum()/trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0,1]

    # evaluate out of sample
    predY = learner.query(testX) # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum()/testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0,1]
    toc = timeit.default_timer()
    print
    print "The run time is ", toc - tic 

    #learners = []
    #for i in range(0,10):
        #kwargs = {"k":i}
        #learners.append(lrl.LinRegLearner(**kwargs))
