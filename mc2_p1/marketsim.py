"""MC2-P1: Market simulator."""

import pandas as pd
import numpy as np
import datetime as dt
import os
from util import get_data, plot_data

def compute_portvals(orders_file = "./orders/orders.csv", start_val = 1000000):
    # define the verbose parameter
    verbose = False
    
    # load the order and sort it by the date
    originalOrders = pd.read_csv(orders_file, index_col = 'Date', parse_dates = True, na_values = ['nan'])
    orders = originalOrders.sort_index(axis = 0)
    if verbose:
        print "Orders: " 
        print orders
        print
    
    # get the start date and the end date
    startDate = orders.index[0]
    endDate = orders.index[-1]
    dateRange = pd.date_range(startDate, endDate)
    
    # get the prices
    stocks = list(set(orders.Symbol.values))
    prices = get_data(stocks, dateRange, True)
    if verbose:
        print "Prices: " 
        print prices
        print
    
    # create a state machine tracking the shares
    shares = dict.fromkeys(stocks, 0)
    if verbose:
        print "Initial Shares: " 
        print shares
        print
    
    # create the output portfolio
    portvals = pd.DataFrame(data = np.zeros((prices.shape[0], 1)), columns = ['Value'], index = prices.index)
    if verbose:
        print "Initial Portfolio: " 
        print portvals
        print
        
    # iterate over all dates when the stocks were traded
    cash = start_val
    for tradeDate, _ in prices.iterrows():
        # Calculate the portfolio value if no orders were placed.
        if tradeDate not in orders.index:
            portval = cash
            for stock in stocks:
                price = prices.get_value(index = tradeDate, col = stock)
                portval += (shares[stock] * price)
            portvals.set_value(index = tradeDate, col = 'Value', value = portval)

            if verbose:
                print "Portfolio value at {} is {}".format(tradeDate, portvals.get_value(index = tradeDate, col = 'Value'))

            continue
            
        # Calculate the portfolio value if any orders were placed.
        portval = 0.0
        for k in range(0, orders.shape[0]):
            orderDate = orders.index[k]
            if orderDate != tradeDate:
                continue

            # retrive information from the orders
            stock = orders.values[k, 0]
            orderShares = orders.values[k, 2]
            order = orders.values[k, 1]
            price = prices.get_value(index = orderDate, col = stock)
            
            # reject the order if the stock was not traded
            if pd.isnull(price):
                continue
            
            oldLeverage, oldTempPortVal, _ = calculateLeverage(shares, stocks, prices, stock, cash, 0.0, price, orderDate)  
            
            # calculate the incremental shares
            incrementalShares = 0.0
            if order == 'BUY':
                incrementalShares = orderShares
            else:
                incrementalShares = -orderShares

            # calculate the leverage
            newLeverage, tempPortVal, tempCash = calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate)           
            
            # reject the order if the leverage exceeds 2.0
            if newLeverage > 2.0 and newLeverage > oldLeverage:
                portval = oldTempPortVal
                continue
         
            # update the portfolio value by accepting the order
            shares[stock] += incrementalShares
            portval = tempPortVal
            cash = tempCash
        
        # The final portfolio value 
        portval += cash
        portvals.set_value(index = tradeDate, col = 'Value', value = portval)
        
        if verbose:
            print "Portfolio value at {} is {}".format(tradeDate, portvals.get_value(index = tradeDate, col = 'Value'))
                            
    return portvals

def calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate):
    # make a copy of the shares
    tempShares = shares.copy()
    
    # deal with ori
    if incrementalShares != 0.0:
        tempShares[stock] += incrementalShares
    tempCash = cash - incrementalShares * price 
    
    # calculate the leverage
    shorts = 0.0
    longs = 0.0
    for tempStock in stocks:
        if tempShares[tempStock] < 0.0:
            shorts -= tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
        elif tempShares[tempStock] > 0.0:
            longs += tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
    tempPortVal = longs - shorts
    leverage = (longs + shorts) / (tempPortVal + tempCash)  
    return leverage, tempPortVal, tempCash

def test_code():
    # this is a helper function you can use to test your code
    # note that during autograding his function will not be called.
    # Define input parameters

    of = "./test_cases/orders-10-modified.csv"
    sv = 1000000

    # Process orders
    portvals = compute_portvals(orders_file = of, start_val = sv)
    if isinstance(portvals, pd.DataFrame):
        portvals = portvals[portvals.columns[0]] # just get the first column
    else:
        "warning, code did not return a DataFrame"

    # calculate the cumulative return
    cr = portvals.values[-1] / sv - 1.0

    # calculate the daily return
    nrData = portvals.shape[0]
    dr = portvals.values[1:nrData] / portvals.values[0:nrData - 1] - 1.0
    
    # calculate the average daily return
    adr = dr.mean()
    
    # calculate the standard deviation of daily return
    sddr = dr.std(ddof = 1)

    # calculate the sharpe ratio
    rfr = 0.0
    sf = 252.0
    sr = np.sqrt(sf) * (adr - rfr) / sddr
    
    # calculate information for display
    start_date = portvals.index[0]
    end_date = portvals.index[-1]
    cum_ret, avg_daily_ret, std_daily_ret, sharpe_ratio = [cr, adr, sddr, sr]

    # Compare portfolio against $SPX
    print "Processing Test Case ", of
    print "Date Range: {} to {}".format(start_date, end_date)
    print "Sharpe Ratio of Fund: {}".format(sharpe_ratio)
    print "Cumulative Return of Fund: {}".format(cum_ret) 
    print "Standard Deviation of Fund: {}".format(std_daily_ret) 
    print "Average Daily Return of Fund: {}".format(avg_daily_ret)
    print "Final Portfolio Value: {}".format(portvals.values[-1])

if __name__ == "__main__":
    test_code()
