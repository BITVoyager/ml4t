import os

import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

def MACDStrategy(stockNames):
    # define verbose 
    verbose = False;

    # create the time range
    startDate = dt.date(2007, 12, 31)
    endDate = dt.date(2009, 12, 31)
    dateRange = pd.date_range(startDate, endDate)

    # load stocks
    originalStocks = get_data(stockNames, dateRange, True)
    originalStocks = originalStocks.drop(labels = 'SPY', axis = 1)  # drop SPY

    # calculate the MACD and the signal
    # This is in Pandas 0.17.0 convention. In 0.18.0, it was changed.
    EMA12 = calculateEMA(12, originalStocks)
    EMA26 = calculateEMA(26, originalStocks)
    MACD = EMA12 - EMA26
    signal = calculateEMA(9, MACD)
    MACD = MACD[34:]
    signal = signal[34:]
    print signal
    print MACD

    # drop the corresponding rows of stocks
    orders = pd.DataFrame(index = MACD.index, columns = ['Symbol', 'Order', 'Shares', 'Type'])

    # set up parameters for Bollinger strategy
    shares = 0  # track number of shares one owns
    
    # apply the bollinger strategy here
    isFirstDay = True   # index of the iteration
    prevTradingDay = None   # initialize the date of prevTradingDay
    for currTradingDay, _ in MACD.iterrows():
        # skip the first day
        if isFirstDay:
            prevTradingDay = currTradingDay
            isFirstDay = False
            continue;
    
        # retrive prices
        currMACD = MACD.get_value(index = currTradingDay, col = stockNames[0])
        prevMACD = MACD.get_value(index = prevTradingDay, col = stockNames[0])

        currSignal = signal.get_value(index = currTradingDay, col = stockNames[0])
        prevSignal = signal.get_value(index = prevTradingDay, col = stockNames[0])

        # long entry
        if shares == 0 and prevMACD < prevSignal and currMACD > currSignal:
            shares = 100
            orders.set_value(index = currTradingDay, col = 'Symbol', value = stockNames[0])
            orders.set_value(index = currTradingDay, col = 'Order', value = 'BUY')
            orders.set_value(index = currTradingDay, col = 'Shares', value = 100)
            orders.set_value(index = currTradingDay, col = 'Type', value = 'Long Entry')
            if verbose:
                print "BUY 100 on ", currTradingDay

        # long exit
        if shares == 100 and prevMACD > prevSignal and currMACD < currSignal:
            shares = 0
            orders.set_value(index = currTradingDay, col = 'Symbol', value = stockNames[0])
            orders.set_value(index = currTradingDay, col = 'Order', value = 'SELL')
            orders.set_value(index = currTradingDay, col = 'Shares', value = 100)
            orders.set_value(index = currTradingDay, col = 'Type', value = 'Long Exit')
            if verbose:
                print "SELL 100 on ", currTradingDay
        
        # short entry
        if shares == 0 and prevMACD > prevSignal and currMACD < currSignal:
            shares = -100
            orders.set_value(index = currTradingDay, col = 'Symbol', value = stockNames[0])
            orders.set_value(index = currTradingDay, col = 'Order', value = 'SELL')
            orders.set_value(index = currTradingDay, col = 'Shares', value = 100)
            orders.set_value(index = currTradingDay, col = 'Type', value = 'Short Entry')
            if verbose:
                print "SELL 100 on ", currTradingDay

        # short exit
        if shares == -100 and prevMACD < prevSignal and currMACD > currSignal:
            shares = 0
            orders.set_value(index = currTradingDay, col = 'Symbol', value = stockNames[0])
            orders.set_value(index = currTradingDay, col = 'Order', value = 'BUY')
            orders.set_value(index = currTradingDay, col = 'Shares', value = 100)
            orders.set_value(index = currTradingDay, col = 'Type', value = 'Short Exit')
            if verbose:
                print "BUY 100 on ", currTradingDay
        
        # update date
        prevTradingDay = currTradingDay
    
    # write orders to the .csv file
    orders = orders.dropna(axis = 0, how = 'any')
    orders4Save = orders.drop(labels = 'Type', axis = 1)
    orders4Save.to_csv(path_or_buf = 'orders.csv', index_label = 'Date')

    # return 
    return originalStocks, MACD, signal, orders

def calculateEMA(windowSize, data):
    SMA = pd.rolling_mean(data, windowSize) / windowSize
    w = 1.0 / (windowSize + 1.0)

    EMA = SMA.copy()
    if EMA.shape[1] != 1:
        EMA = EMA.drop(labels = '$SPX', axis = 1)
    print EMA.shape
    
    for k in range(windowSize, EMA.shape[0]):
        val = (data.values[k, 0] - EMA.values[k - 1, 0]) * w + EMA.values[k - 1, 0]
        EMA.set_value(index = EMA.index[k], col = EMA.columns[0], value = val)

    return EMA
        

def computePortVals(ordersFile, startPortVal):
    # define the verbose parameter
    verbose = False 
    
    # load the order and sort it by the date
    originalOrders = pd.read_csv(ordersFile, index_col = 'Date', parse_dates = True, na_values = ['nan'])
    orders = originalOrders.sort_index(axis = 0)
    if verbose:
        print "Orders: " 
        print orders
        print
    
    # get the start date and the end date
    startDate = orders.index[0]
    endDate = orders.index[-1]
    dateRange = pd.date_range(startDate, endDate)
    
    # get the prices
    stocks = list(set(orders.Symbol.values))
    prices = get_data(stocks, dateRange, True)
    if verbose:
        print "Prices: " 
        print prices
        print
    
    # create a state machine tracking the shares
    shares = dict.fromkeys(stocks, 0)
    if verbose:
        print "Initial Shares: " 
        print shares
        print
    
    # create the output portfolio
    portVals = pd.DataFrame(data = np.zeros((prices.shape[0], 1)), columns = ['Value'], index = prices.index)
    if verbose:
        print "Initial Portfolio: " 
        print portVals
        print
        
    # iterate over all dates when the stocks were traded
    cash = startPortVal
    for tradeDate, _ in prices.iterrows():
        # Calculate the portfolio value if no orders were placed.
        if tradeDate not in orders.index:
            portval = cash
            for stock in stocks:
                price = prices.get_value(index = tradeDate, col = stock)
                portval += (shares[stock] * price)
            portVals.set_value(index = tradeDate, col = 'Value', value = portval)

            if verbose:
                print "Portfolio value at {} is {}".format(tradeDate, portVals.get_value(index = tradeDate, col = 'Value'))

            continue
            
        # Calculate the portfolio value if any orders were placed.
        portval = 0.0
        for k in range(0, orders.shape[0]):
            orderDate = orders.index[k]
            if orderDate != tradeDate:
                continue

            # retrive information from the orders
            stock = orders.values[k, 0]
            orderShares = orders.values[k, 2]
            order = orders.values[k, 1]
            price = prices.get_value(index = orderDate, col = stock)
            
            # reject the order if the stock was not traded
            if pd.isnull(price):
                continue
            
            oldLeverage, oldTempPortVal, _ = calculateLeverage(shares, stocks, prices, stock, cash, 0.0, price, orderDate)  
            
            # calculate the incremental shares
            incrementalShares = 0.0
            if order == 'BUY':
                incrementalShares = orderShares
            else:
                incrementalShares = -orderShares

            # calculate the leverage
            newLeverage, tempPortVal, tempCash = calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate)           
            
            # reject the order if the leverage exceeds 2.0
            if newLeverage > 2.0 and newLeverage > oldLeverage:
                portval = oldTempPortVal
                continue
         
            # update the portfolio value by accepting the order
            shares[stock] += incrementalShares
            portval = tempPortVal
            cash = tempCash
        
        # The final portfolio value 
        portval += cash
        portVals.set_value(index = tradeDate, col = 'Value', value = portval)
        
        if verbose:
            print "Portfolio value at {} is {}".format(tradeDate, portVals.get_value(index = tradeDate, col = 'Value'))
                            
    return portVals

def calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate):
    # make a copy of the shares
    tempShares = shares.copy()
    
    # deal with ori
    if incrementalShares != 0.0:
        tempShares[stock] += incrementalShares
    tempCash = cash - incrementalShares * price 
    
    # calculate the leverage
    shorts = 0.0
    longs = 0.0
    for tempStock in stocks:
        if tempShares[tempStock] < 0.0:
            shorts -= tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
        elif tempShares[tempStock] > 0.0:
            longs += tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
    tempPortVal = longs - shorts
    leverage = (longs + shorts) / (tempPortVal + tempCash)  
    return leverage, tempPortVal, tempCash

def assessPortfolio(dateRange, portVals, originalStocks, MACD, signal, orders):
    # slice the portfolio values    
    portVals = portVals[portVals.columns[0]][dateRange] # just get the first column 
    portVals = portVals.dropna(axis = 0, how = 'any')   # remove non-trading dates  

    # slice $SPX
    SPX = originalStocks[originalStocks.columns[1]][dateRange]
    SPX = SPX.dropna(axis = 0, how = 'any') 

    assert(SPX.shape[0] == portVals.shape[0])

    # calculate the cumulative return
    cr = portVals.values[-1] / portVals.values[0] - 1.0
    crSPX = SPX.values[-1] / SPX.values[0] - 1.0

    # calculate the daily return
    nrData = portVals.shape[0]
    dr = portVals.values[1:nrData] / portVals.values[0:nrData - 1] - 1.0
    drSPX = SPX.values[1:nrData] / SPX.values[0:nrData - 1] - 1.0
    
    # calculate the average daily return
    adr = dr.mean()
    adrSPX = drSPX.mean()
    
    # calculate the standard deviation of daily return
    sddr = dr.std(ddof = 1)
    sddrSPX = drSPX.std(ddof = 1)

    # calculate the sharpe ratio
    rfr = 0.0
    sf = 252.0
    sr = np.sqrt(sf) * (adr - rfr) / sddr
    srSPX = np.sqrt(sf) * (adrSPX - rfr) / sddrSPX
    
    # calculate information for display
    startDate = portVals.index[0]
    endDate = portVals.index[-1]

    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(startDate, endDate)
    print
    print "Sharpe Ratio of Fund: {}".format(sr)
    print "Sharpe Ratio of $SPX: {}".format(srSPX)
    print
    print "Cumulative Return of Fund: {}".format(cr) 
    print "Cumulative Return of $SPX: {}".format(crSPX) 
    print
    print "Standard Deviation of Fund: {}".format(sddr) 
    print "Standard Deviation of $SPX: {}".format(sddrSPX) 
    print
    print "Average Daily Return of Fund: {}".format(adr)
    print "Average Daily Return of $SPX: {}".format(adrSPX)
    print
    print "Final Portfolio Value: {}".format(portVals.values[-1])

    # plot
    actions = orders[orders.columns[3]][dateRange] # just get the first column 
    actions = actions.dropna(axis = 0, how = 'any')   # remove non-trading dates  
    
    stocks = originalStocks.drop(labels = '$SPX', axis = 1) 
    MACD = MACD.drop(labels = '$SPX', axis = 1)
    MACD.columns = ['MACD']
    signal = signal.drop(labels = '$SPX', axis = 1)
    signal.columns = ['signal']

    concatDataFrame1 = pd.concat(objs = [stocks, MACD, signal], axis = 1)
    ax1 = concatDataFrame1.plot(title = 'Stock Price', fontsize = 12)
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Price')
    for date, _ in actions.iteritems():
        action = actions.get_value(date) 
        if action == 'Long Entry':
            ax1.axvline(date, color = 'g', linestyle='-', lw = 1)
        if action == 'Short Entry':
            ax1.axvline(date, color = 'r', linestyle='-', lw = 1)
        if action == 'Long Exit' or action == 'Short Exit':
            ax1.axvline(date, color = 'k', linestyle='-', lw = 1)
    
    # plot normalized portfolio value and normalized $SPX
    portVals = portVals / portVals.values[0]
    concatDataFrame2 = pd.concat(objs = [portVals / portVals.values[0], SPX / SPX.values[0]], axis = 1)
    concatDataFrame2.columns = ['Portfolio', '$SPX']
    ax2 = concatDataFrame2.plot(title = 'Daily portfolio value and $SPX', fontsize = 12)
    ax2.set_xlabel('Date')
    ax2.set_ylabel('Normalized price')
    plt.show()

#####################################################################
## Functions copied from util.py
#####################################################################
def symbol_to_path(symbol, base_dir=os.path.join("..", "data")):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))


def get_data(symbols, dates, addSPY=True):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if addSPY and 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols = ['SPY'] + symbols

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])
    return df
   
if __name__ == "__main__":
    # set up parameters
    stocks = ['IBM', '$SPX']
    windowSize = 20
    
    # apply Bollinger Strategy
    originalStocks, MACD, signal, orders = MACDStrategy(stocks)
    
    # apply market simulater
    ordersFile = 'orders.csv'
    startPortVal = 10000
    portVals = computePortVals(ordersFile, startPortVal)
    
    # assess portfolio
    assessStartDate = orders.index[0]
    assessEndDate = orders.index[-1]
    assessDateRange = pd.date_range(assessStartDate, assessEndDate)
    assessPortfolio(assessDateRange, portVals, originalStocks, MACD, signal, orders)
