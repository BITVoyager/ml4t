"""
@brief  Template for implementing QLearner  (c) 2015 Tucker Balch
@author Tucker Balch
@author Yao Chen
@date   July 25, 2016
"""

import numpy as np
import random as rand

class QLearner(object):
    """
    @brief  Initialize the class
    @param  num_states: number of states
    @param  num_actions: number of actions
    @param  alpha: learning rate
    @parmm  gamma: discount rate
    @param  rar: random action rate
    @param  radr: random action decay rate
    @param  dyna: number of Dyna updates
    @param  verbose: flag for displaying debugging statements        
    """
    def __init__(self, num_states = 100, num_actions = 4, alpha = 0.2, gamma = 0.9, rar = 0.5, radr = 0.99, dyna = 0, verbose = False):    
        # parse parameters    
        self.num_states = num_states
        self.num_actions = num_actions
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar
        self.radr = radr
        self.dyna = dyna
        self.verbose = verbose
        
        # initialize state and action
        self.s = 0
        self.a = 0
        
        # initialize the Q table
        self.Q = np.random.uniform(low = -1.0, high = 1.0, size = (num_states, num_actions))

        # initialize R and T matrices for Dyna
        if dyna != 0:
            self.Tc = np.ones(shape = (num_states, num_actions, num_states)) * 1e-4
            self.R = np.random.uniform(low = -1.0, high = 1.0, size = (num_states, num_actions))
            self.T = {}

    def querysetstate(self, s):
        """
        @brief   Update the state without updating the Q-table
        @param   s: The new state
        @return  The selected action
        """
        # update state
        self.s = s
        
        # draw an action
        # Do not update rar!
        sample = np.random.binomial(1, self.rar)
        action = -1
        if sample == 1:
            action = rand.randint(0, self.num_actions - 1)
        else:
            action = self.Q[s, :].argmax()        
        
        # show the query result
        if self.verbose: 
            print "s = ", s, "a = ", action
        
        # return the action
        return action

    def query(self, s_prime, r):
        """
        @brief   Update the Q table and return an action
        @param   s_prime: new state
        @param   r: immediate reward
        @return  The selected action
        """
        # update the Q table        
        value1 = (1.0 - self.alpha) * self.Q[self.s, self.a]   
        value2 = self.alpha * (r + self.gamma * self.Q[s_prime].max())
        self.Q[self.s, self.a] = value1 + value2

        # draw an action
        sample = np.random.binomial(1, self.rar)
        action = -1
        if sample == 1:
            action = rand.randint(0, self.num_actions - 1)
        else:
            action = self.Q[s_prime, :].argmax()   

        # apply Dyna
        if self.dyna != 0:
            # update R
            self.R[self.s, self.a] =  (1 - self.alpha) * self.R[self.s, self.a] + self.alpha * r

            # update T
            self.Tc[self.s, self.a, s_prime] =  self.Tc[self.s, self.a, s_prime] + 1
            self.T[self.s] = {self.a: {s_prime: self.Tc[self.s, self.a, s_prime] / self.Tc[self.s, self.a, :].sum()}}

            sss = np.random.randint(low = 0, high = self.num_states, size = (self.dyna, 1))
            aaa = np.random.randint(low = 0, high = self.num_actions, size = (self.dyna, 1))

            for k in range(0, self.dyna):
                ss = int(sss[k])
                aa = int(aaa[k])
                if ss in self.T:
                    if aa in self.T[ss]:
                        temp = np.array(self.T[ss][aa].items())
                        ssp = np.random.choice(a = temp[:, 0])               
                        rr = self.R[ss, aa]
                        value3 = (1.0 - self.alpha) * self.Q[ss, aa]   
                        value4 = self.alpha * (rr + self.gamma * self.Q[ssp].max())
                        self.Q[ss, aa] = value3 + value4

        # update state and action
        self.s = s_prime
        self.a = action

        # decay rar by radr
        self.rar = self.rar * self.radr  
        
        # show the query result
        if self.verbose: 
            print "s = ", s_prime, " a = ", action," r = ", r
        
        # return the action
        return action

if __name__=="__main__":
    print "Remember Q from Star Trek? Well, this isn't him"
