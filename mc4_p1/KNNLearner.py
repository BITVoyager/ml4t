"""
@brief  This class implements a k-Nearest Neighbors (kNN) learner for regression.
@author Yao Chen
@date   2016-06-11
"""

import numpy as np

class KNNLearner(object):
	
    def __init__(self, k = 3, verbose = False):
        """
        @brief  Initialize parameters.
        @param  k: the number of nearest neighbors
        @param  verbose: the flag of printing debug information
        """

        self.k = k
        self.verbose = verbose

    def addEvidence(self, trainX, trainY):
        """
        @brief  Train the model.
        @param  trainX: X values of the training data
        @param  trainY: Y values of the training data
        """

        # Since kNN is a "lazy" learner and we do not use kd-tree, the training data are just stored as they are.
        self.trainX = trainX
        self.trainY = trainY
        
    def query(self, testX):
        """
        @brief	Estimate a set of test points given the kNN model.
        @param 	testX: the input points for testing
        @return	testY: the estimated values according to the kNN model
        """
	
        # calculate the predictions (testY) using vectorization
        testY = np.zeros(testX.shape[0])
        for i in range(0, testX.shape[0]):
            # calculate the squared Euclidean distances
            dists = np.square(self.trainX - testX[i]).sum(axis = 1)
	
            # sort the distances in ascending order
            sortedIndices = dists.argsort()

            # calculate the prediction based on the model
            testY[i] = self.trainY[sortedIndices[0:self.k]].sum() / self.k

        # return the predictions
        return testY

