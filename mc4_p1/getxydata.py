"""
@brief  This function generates features and y's
@author Yao Chen
@date   2016-07-11
"""

import pandas as pd
import numpy as np
import datetime as dt

from util import get_data

def getXYData(dateRange, windowSize, stock):
    """
    @brief      creates technical features and correponding y's
    @param      dateRange: range of dates for training/testing
    @param      windowSize: size of the rolling window
    @param      stock: the name of the stock that we want to analyze
    @return     X: technical features (Momentum, Bollinger value, rolling standard deviation) 
    @return     Y: 5-day cumulative returns
    """
    # define verbose
    verbose = False;

    # get stock price
    price = get_data([stock], dateRange, True)
    price = price.drop(labels = 'SPY', axis = 1)  # drop SPY

    # get $SPX
    SPX = get_data(['$SPX'], dateRange, True)
    SPX = SPX.drop(labels = 'SPY', axis = 1)  # drop SPY
    
    # calculate the momentum
    # momentum[t] = (price[t] / price[t-N]) - 1
    momentum = price / price.shift(periods = windowSize - 1) - 1.0
    if verbose:
        print "Momentum: ", momentum

    # calculate rolling standard deviation
    # sigma[t] = std(price[t-N], price[t])
    sigma = pd.rolling_std(price, windowSize)
    if verbose:
        print "rolling standard deviation: ", sigma
    
    # calculate Bollinger Bands
    # bb_value[t] = (price[t] - SMA[t]) / (2 * stddev[t])
    SMA = pd.rolling_mean(price, windowSize)
    bbValue = (price - SMA) / (2.0 * sigma) 
    if verbose:
        print "Bollinger value: ", bbValue

    # calculate the Y
    Y = price.shift(periods = -5) / price - 1.0
    if verbose:
        print "Y: ", Y

    # trainX
    trainX = pd.concat([momentum, bbValue, sigma], axis = 1)
    trainX.columns = ['Momentum', 'BB', 'Votality']

    # trainY
    trainY = Y
    trainY.colums = ['Prediction']

    return trainX, trainY, price, SPX

if __name__ == "__main__":
    # set up parameters
    stock = 'ML4T-240'
    windowSize = 20

    startDate = dt.date(2007, 12, 31)
    endDate = dt.date(2009, 12, 31)
    dateRange = pd.date_range(startDate, endDate)

    trainX, trainY = getXYData(dateRange, windowSize, stock)
    #print trainX
    #print trainY
