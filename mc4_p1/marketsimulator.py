"""
@brief  A simple stock market simulator.
@author Yao Chen
@date   2016-07-11
"""

import os

import pandas as pd
import numpy as np
import datetime as dt

from util import get_data

def computePortVals(ordersFile, startPortVal):
    """
    @brief      compute a series of the portfolio values given initial portfolio value and a series of orders
    @param      ordersFile: the filename of the order
    @param      startPortVal: the initial value of the portfolio
    @return     a series portfolio values
    """

    # define the verbose parameter
    verbose = False 
    
    # load the order and sort it by the date
    originalOrders = pd.read_csv(ordersFile, index_col = 'Date', parse_dates = True, na_values = ['nan'])
    orders = originalOrders.sort_index(axis = 0)
    if verbose:
        print "Orders: " 
        print orders
        print
    
    # get the start date and the end date
    startDate = orders.index[0]
    endDate = orders.index[-1]
    dateRange = pd.date_range(startDate, endDate)
    
    # get the prices
    stocks = list(set(orders.Symbol.values))
    prices = get_data(stocks, dateRange, True)
    if verbose:
        print "Prices: " 
        print prices
        print
    
    # create a state machine tracking the shares
    shares = dict.fromkeys(stocks, 0)
    if verbose:
        print "Initial Shares: " 
        print shares
        print
    
    # create the output portfolio
    portVals = pd.DataFrame(data = np.zeros((prices.shape[0], 1)), columns = ['Value'], index = prices.index)
    if verbose:
        print "Initial Portfolio: " 
        print portVals
        print
        
    # iterate over all dates when the stocks were traded
    cash = startPortVal
    for tradeDate, _ in prices.iterrows():
        # Calculate the portfolio value if no orders were placed.
        if tradeDate not in orders.index:
            portval = cash
            for stock in stocks:
                price = prices.get_value(index = tradeDate, col = stock)
                portval += (shares[stock] * price)
            portVals.set_value(index = tradeDate, col = 'Value', value = portval)

            if verbose:
                print "Portfolio value at {} is {}".format(tradeDate, portVals.get_value(index = tradeDate, col = 'Value'))

            continue
            
        # Calculate the portfolio value if any orders were placed.
        portval = 0.0
        for k in range(0, orders.shape[0]):
            orderDate = orders.index[k]
            if orderDate != tradeDate:
                continue

            # retrive information from the orders
            stock = orders.values[k, 0]
            orderShares = orders.values[k, 2]
            order = orders.values[k, 1]
            price = prices.get_value(index = orderDate, col = stock)
            
            # reject the order if the stock was not traded
            if pd.isnull(price):
                continue
            
            oldLeverage, oldTempPortVal, _ = calculateLeverage(shares, stocks, prices, stock, cash, 0.0, price, orderDate)  
            
            # calculate the incremental shares
            incrementalShares = 0.0
            if order == 'BUY':
                incrementalShares = orderShares
            else:
                incrementalShares = -orderShares

            # calculate the leverage
            newLeverage, tempPortVal, tempCash = calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate)           
            
            # reject the order if the leverage exceeds 2.0
            if newLeverage > 1e10 and newLeverage > oldLeverage:
                portval = oldTempPortVal
                continue
         
            # update the portfolio value by accepting the order
            shares[stock] += incrementalShares
            portval = tempPortVal
            cash = tempCash
        
        # The final portfolio value 
        portval += cash
        portVals.set_value(index = tradeDate, col = 'Value', value = portval)
        
        if verbose:
            print "Portfolio value at {} is {}".format(tradeDate, portVals.get_value(index = tradeDate, col = 'Value'))
                            
    return portVals

def calculateLeverage(shares, stocks, prices, stock, cash, incrementalShares, price, orderDate):
    """
    @brief      calculate leverage
    @return     the current leverage
    """

    # make a copy of the shares
    tempShares = shares.copy()
    
    # deal with ori
    if incrementalShares != 0.0:
        tempShares[stock] += incrementalShares
    tempCash = cash - incrementalShares * price 
    
    # calculate the leverage
    shorts = 0.0
    longs = 0.0
    for tempStock in stocks:
        if tempShares[tempStock] < 0.0:
            shorts -= tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
        elif tempShares[tempStock] > 0.0:
            longs += tempShares[tempStock] * prices.get_value(index = orderDate, col = tempStock)
    tempPortVal = longs - shorts
    leverage = (longs + shorts) / (tempPortVal + tempCash)  
    return leverage, tempPortVal, tempCash
