"""
@brief  This function implements the backtest pipeline
@author Yao Chen
@date   2016-07-11
"""

import os

import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

from getxydata import getXYData
from marketsimulator import computePortVals
from KNNLearner import KNNLearner
from util import get_data, plot_data

def backTest(trainDateRange, testDateRange, stock, isInSampleTesting):
    # define verbose
    verbose = False
    
    # 1. get training and testing data
    windowSize = 50

    originalTrainX, originalTrainY, _, _ = getXYData(trainDateRange, windowSize, stock)
    trainX = originalTrainX[windowSize - 1:originalTrainX.shape[0] - 5]
    trainY = originalTrainY[windowSize - 1:originalTrainY.shape[0] - 5]

    originalTestX, originalTestY, price, SPX = getXYData(testDateRange, windowSize, stock)
    testX = originalTestX[windowSize - 1:originalTestX.shape[0] - 5]
    testY = originalTestY[windowSize - 1:originalTestY.shape[0] - 5]
    
    # 2. predict Y
    learner = KNNLearner(k = 7, verbose = False)
    learner.addEvidence(trainX.values, trainY.values)

    predictedTrainY = learner.query(trainX.values)
    predictedTestY = learner.query(testX.values)

    # 3. create orders based on strategy
    orders = pd.DataFrame(index = originalTestX.index, columns = ['Symbol', 'Order', 'Shares', 'Type'])
    shares = 0  # track number of shares one owns
    k = 0       # index of date

    for date, _ in originalTestX.iterrows():
        # (1) skip the first few days and last few days
        if k < windowSize - 1:
            k = k + 1
            continue

        if k - windowSize + 1 == predictedTestY.shape[0]:
            break
       
        # (2-1) long entry: no shares and positive 5-day cumulative returns 
        if shares == 0 and predictedTestY[k - windowSize + 1] > 0.0:
            shares = 100
            orders.set_value(index = date, col = 'Symbol', value = stock)
            orders.set_value(index = date, col = 'Order', value = 'BUY')
            orders.set_value(index = date, col = 'Shares', value = 100)
            orders.set_value(index = date, col = 'Type', value = 'Long Entry')
            if verbose:
                print "BUY Long 100 on ", date
            k = k + 1
            continue

        # (2-2) long exit: 100 long shares and negative 5-day cumulative returns 
        if shares == 100 and predictedTestY[k - windowSize + 1] < 0.0:
            shares = 0
            orders.set_value(index = date, col = 'Symbol', value = stock)
            orders.set_value(index = date, col = 'Order', value = 'SELL')
            orders.set_value(index = date, col = 'Shares', value = 100)
            orders.set_value(index = date, col = 'Type', value = 'Long Exit')
            if verbose:
                print "SELL Long 100 on ", date
            k = k + 1
            continue
        
        # (2-3) short entry: no shares and negative 5-day cumulative returns 
        if shares == 0 and predictedTestY[k - windowSize + 1] < 0.0:
            shares = -100
            orders.set_value(index = date, col = 'Symbol', value = stock)
            orders.set_value(index = date, col = 'Order', value = 'SELL')
            orders.set_value(index = date, col = 'Shares', value = 100)
            orders.set_value(index = date, col = 'Type', value = 'Short Entry')
            if verbose:
                print "SELL Short 100 on ", date
            k = k + 1
            continue

        # (2-4) short exit: 100 short shares and positive 5-day cumulative returns 
        if shares == -100 and predictedTestY[k - windowSize + 1] > 0.0:
            shares = 0
            orders.set_value(index = date, col = 'Symbol', value = stock)
            orders.set_value(index = date, col = 'Order', value = 'BUY')
            orders.set_value(index = date, col = 'Shares', value = 100)
            orders.set_value(index = date, col = 'Type', value = 'Short Exit')
            if verbose:
                print "BUY Short 100 on ", date
            k = k + 1
            continue
        
        k = k + 1
    
    # 4. save orders
    orders = orders.dropna(axis = 0, how = 'any')
    orders4Save = orders.drop(labels = 'Type', axis = 1)
    orders4Save.to_csv(path_or_buf = 'orders.csv', index_label = 'Date')

    # 5. simulate the strategy using the saved order file
    ordersFile = 'orders.csv'
    startPortVal = 10000
    portVals = computePortVals(ordersFile, startPortVal)

    # 6. assess portfolio
    assessStartDate = orders.index[0]
    assessEndDate = orders.index[-1]
    assessDateRange = pd.date_range(assessStartDate, assessEndDate)
    assessPortfolio(assessDateRange, portVals, orders)

    # 7. plot results
    if stock == 'ML4T-240' and isInSampleTesting:
        predictedTrainYDF = pd.DataFrame(predictedTestY, index = testY.index)   
        plotResults1(price, trainY, predictedTrainYDF)

    plotResults2(price, orders, isInSampleTesting)
    plotResults3(stock, portVals, SPX, isInSampleTesting)

def assessPortfolio(dateRange, portVals, orders):
    
    # slice the portfolio values    
    portVals = portVals[portVals.columns[0]][dateRange] # just get the first column 
    portVals = portVals.dropna(axis = 0, how = 'any')   # remove non-trading dates  

    # calculate the cumulative return
    cr = portVals.values[-1] / portVals.values[0] - 1.0

    # calculate the daily return
    nrData = portVals.shape[0]
    dr = portVals.values[1:nrData] / portVals.values[0:nrData - 1] - 1.0
    
    # calculate the average daily return
    adr = dr.mean()

    # calculate the standard deviation of daily return
    sddr = dr.std(ddof = 1)

    # calculate the sharpe ratio
    rfr = 0.0
    sf = 252.0
    sr = np.sqrt(sf) * (adr - rfr) / sddr
    
    # calculate information for display
    startDate = portVals.index[0]
    endDate = portVals.index[-1]

    # Compare portfolio against $SPX
    print "Date Range: {} to {}".format(startDate, endDate)
    print
    print "Sharpe Ratio of Fund: {}".format(sr)
    print
    print "Cumulative Return of Fund: {}".format(cr) 
    print
    print "Standard Deviation of Fund: {}".format(sddr) 
    print
    print "Average Daily Return of Fund: {}".format(adr)

def plotResults1(price, trainY, predictedTrainY):
    trainY = (1.0 + trainY) * price
    predictedTrainY.columns = trainY.columns
    predictedTrainY = (1.0 + predictedTrainY) * price
    
    df = pd.concat(objs = [price, trainY, predictedTrainY], axis = 1);
    df.columns = ['Price', 'Training Y', 'Predicted Y']
    plot_data(df, title = price.columns[0] + " In-sample Training Y/Price/Predicted Y", xlabel = "Date", ylabel = "Price")

def plotResults2(price, orders, isInSampleTesting):
    if isInSampleTesting:
        title = price.columns[0] + ' In-sample Entries/Exits'
    else:
        title = price.columns[0] + ' Out-of-sample Entries/Exits'
    ax = price.plot(title = title, fontsize = 12)
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')    
    actions = orders[orders.columns[3]]
    for date, _ in actions.iteritems():  
        action = actions.get_value(date) 
        if action == 'Long Entry':
            ax.axvline(date, color = 'g', linestyle='-', lw = 1)
        if action == 'Short Entry':
            ax.axvline(date, color = 'r', linestyle='-', lw = 1)
        if action == 'Long Exit' or action == 'Short Exit':
            ax.axvline(date, color = 'k', linestyle='-', lw = 1)

def plotResults3(stock, portVals, SPX, isInSampleTesting):
    portVals = portVals / portVals.values[0]
    portVals.columns = [stock]
    SPX = SPX / SPX.values[0]
    df = SPX.join(portVals)    
    if isInSampleTesting:
        plot_data(df, title = stock + " In-sample Back Test", xlabel = "Date", ylabel = "Price") 
    else:
        plot_data(df, title = stock + " Out-of-sample Back Test", xlabel = "Date", ylabel = "Price")        

if __name__ == "__main__":
    # set up stock
    stock1 = 'ML4T-240'
    stock2 = 'IBM'
    
    # set up date ranges
    startDate = dt.date(2007, 12, 31)
    endDate = dt.date(2009, 12, 31)
    trainDateRange = pd.date_range(startDate, endDate)

    startDate = dt.date(2009, 12, 31)
    endDate = dt.date(2011, 12, 31)
    testDateRange = pd.date_range(startDate, endDate)
    
    # apply in-sample back testing for ML4T-240
    backTest(trainDateRange, trainDateRange, stock1, True)

    # apply out-of-sample back testing for ML4T-240
    backTest(trainDateRange, testDateRange, stock1, False)

    # apply in-sample back testing for IBM
    backTest(trainDateRange, trainDateRange, stock2, True)

    # apply out-of-sample back testing for IBM
    backTest(trainDateRange, testDateRange, stock2, False)



    plt.show()
    
    
