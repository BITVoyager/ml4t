1. There are 5 files involved.
    backtest.py         --backtest
    getxydata.py        --get the indicators and y's
    KNNLearner.py       --a class of KNN learner
    util.py             --always there to give utility functions
    marketsimulators.py --market simulator
2. How to run the code?
    cd /path/to/ml4t/mc4_p1/
    python backtest.py
    Then, all plots will be shown. You can also see the statistics of the portfolio in the terminal.
3. Potential issues:
    The code has been tested on my machines (2 different linux machines with same configuration) and it works well. 
    I tested it on buffet01. I can get the results printed out but cannot get the plots. I think this might be due to some restrictions.
    I cannot get the plots in mc1_p1/ when execuating python analysis.py either, which I could have done weeks ago.
4. Contact:
    If you need any clarification, feel free to contact Yao via yao.chen@gatech.edu.
    
