"""MC1-P1: Analyze a portfolio."""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from util import get_data, plot_data

# This is the function that will be tested by the autograder
# The student must update this code to properly implement the functionality
def assess_portfolio(sd = dt.datetime(2008,1,1), ed = dt.datetime(2009,1,1), \
    syms = ['GOOG','AAPL','GLD','XOM'], \
    allocs=[0.1,0.2,0.3,0.4], \
    sv=1000000, rfr=0.0, sf=252.0, \
    gen_plot=False):

    # Read in adjusted closing prices for given symbols, date range
    dates = pd.date_range(sd, ed)
    prices_all = get_data(syms, dates)  # automatically adds SPY
    prices = prices_all[syms]  # only portfolio symbols
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    # Get daily portfolio value
    prices_SPY.ffill()
    prices_SPY.bfill()
    cr_SPY = prices_SPY.values / prices_SPY.values[0]

    # Get portfolio statistics (note: std_daily_ret = volatility)
    prices.ffill()
    prices.bfill()
    cumulative_return_portfolio = prices.values / prices.values[0] * np.array(allocs)
    cr_portfolio = np.sum(cumulative_return_portfolio, axis=1)
    cr = cr_portfolio[-1] - 1.0

    num_data = prices_all.values.shape[0]
    dr_portfolio = cr_portfolio[1:num_data] / cr_portfolio[0:num_data - 1] - 1.0
    adr = dr_portfolio.mean()
    sddr = dr_portfolio.std(ddof=1)

    sr = np.sqrt(sf) * (adr - rfr) / sddr

    # Compare daily portfolio value with SPY using a normalized plot
    if gen_plot:
        # add code to plot here
        index_list_SPY = prices_SPY.index.tolist()
        temp_SPY = pd.DataFrame(data=cr_SPY, index=index_list_SPY, columns=['Portfolio'])

        index_list_portfolio = prices.index.tolist()
        temp_portfolio = pd.DataFrame(data=cr_portfolio, index=index_list_portfolio, columns=['SPY'])

        df_temp = pd.concat([temp_portfolio, temp_SPY], axis=1)
        plot_data(df_temp, title="Daily portfolio value and SPY", xlabel="Date", ylabel="Normalized price")
        pass

    # Add code here to properly compute end value
    ev = cr_portfolio[-1] * sv

    return cr, adr, sddr, sr, ev

def test_code():
    # This code WILL NOT be tested by the auto grader
    # It is only here to help you set up and test your code

    # Define input parameters
    # Note that ALL of these values will be set to different values by
    # the autograder!
    start_date = dt.datetime(2009, 1, 1)
    end_date = dt.datetime(2009, 12, 31)
    symbols = ['GOOG', 'AAPL', 'GLD', 'MSFT']
    allocations = [0.3, 0.3, 0.1, 0.3]
    start_val = 1000000  
    risk_free_rate = 0.0
    sample_freq = 252

    # Assess the portfolio
    cr, adr, sddr, sr, ev = assess_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        allocs = allocations,\
        sv = start_val, \
        gen_plot = True)

    # Print statistics
    print "Start Date:", start_date
    print "End Date:", end_date
    print "Symbols:", symbols
    print "Allocations:", allocations
    print "Sharpe Ratio:", sr
    print "Volatility (stdev of daily returns):", sddr
    print "Average Daily Return:", adr
    print "Cumulative Return:", cr

if __name__ == "__main__":
    test_code()
